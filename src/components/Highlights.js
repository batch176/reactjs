//Highlights.js

import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights(){
	return(

		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>ROG Strix GeForce RTX™ 3050 OC Edition 8GB</h2>
							<img className='rtx3050' src='https://dlcdnwebimgs.asus.com/gain/3472707B-776F-48A1-B2C9-85706A263CBD/w717/h525' />
							
						</Card.Title>

						<Card.Text>
						ROG Strix GeForce RTX™ 3050 OC Edition 8GB GDDR6 buffed-up design with chart-topping thermal performance.
                        NVIDIA Ampere Streaming Multiprocessors: The all-new Ampere SM brings 2X the FP32 throughput and improved power efficiency.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>ASUS Radeon RX 6000 Series Graphics Cards</h2>
							<img className='rx600' src='https://www.asus.com/microsite/graphics-cards/radeon-rx-6000-series/img/banner/banner-tuf-3.png'/>
						</Card.Title>

						<Card.Text>
							Premium MOSFETs, capacitors and chokes are the foundation of the formidable power solution in the ASUS Radeon RX 6000 series. Along with enabling robust cooling, these graphics cards make the most out of the newest 7 nm AMD RDNA™ 2 architecture. They also support up to 4K AAA gaming and Microsoft DirectX, and they provide ray tracing for more realistic in-game lighting. Additional features such as AMD Infinity Cache and AMD Smart Access Memory improve memory efficiency to provide a sizeable boost to frame rates across all display resolutions.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>ROG Strix LC GeForce RTX 3090 Ti OC Edition</h2>
							<img className='rtx3090' src='https://dlcdnwebimgs.asus.com/gain/83B44724-21FD-4925-B01D-FE0D53DEA5BB/w717/h525' />
						</Card.Title>

						<Card.Text>
							ROG Strix LC GeForce RTX™ 3090 Ti OC Edition 24GB GDDR6X reigns supreme by combining liquid cooling with the NVIDIA Ampere architecture.
							NVIDIA Ampere Streaming Multiprocessors: The all-new Ampere SM brings 2X the FP32 throughput and improved power efficiency.
							2nd Generation RT Cores: Experience 2X the throughput of 1st gen RT Cores, plus concurrent RT and shading for a whole new level of ray-tracing performance.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

		</Row>
		)
}