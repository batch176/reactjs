//Register.js

import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Register() {


	const { user } = useContext(UserContext)

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');

	const [ isActive, setIsActive ] = useState(true);

	useEffect(() => {
		if((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [email, password, verifyPassword])


	function registerUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/register', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.message)

			if(data){

				if(data.message === `Email already existed. Please try again?`) {
					Swal.fire({
					title: 'error',
					icon: 'error',
					text: 'Email already existed. Please try again'
					})

				} else {
					Swal.fire({
						title: 'Yaaaaaaaaaaaay!',
						icon: 'success',
						text: 'You have successfully registered!'
					})
					Navigate('/login')
				}

			} 
			else {
				Swal.fire({
					title: 'error',
					icon: 'error',
					text: 'Please try again'
				})
			}

		})

}


	return(

		(user.accessToken !== null) ?

		<Navigate to="/products"/>

		:

		<Form onSubmit={e => registerUser(e)}>
		    <h1>Register</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
				    type="email"
				    placeholder="Enter email"
				    required
				    value={email}
				    onChange={e => setEmail(e.target.value)}
				    />
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				    type="password"
				    placeholder="Enter your Password"
				    required
				    value={password}
				    onChange={e => setPassword(e.target.value)}
				    />
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
				    type="password"
				    placeholder="Verify Password"
				    required
				    value={verifyPassword}
				    onChange={e => setVerifyPassword(e.target.value)}
				    />
			</Form.Group>
			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
			}
			
			
		</Form>



		)
}